<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {		
		$earthRadius = 6371000;
		$latFrom = deg2rad($from[0]);
		$lonFrom = deg2rad($from[1]);
		$latTo = deg2rad($to[0]);
		$lonTo = deg2rad($to[1]);

		$latDelta = $latTo - $latFrom;
		$lonDelta = $lonTo - $lonFrom;
		$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
    	cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  		$distance = round(($angle * $earthRadius) / 1000 ,2);
		return $distance;
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
		$results = [];
		foreach ($offices as $office) {
			$officeInfo = [];
			$to = [];
			$to[] = $office['lat'];
			$to[] =	$office['lng'];
			$distance = $this->calculate($from, $to);
			array_push($results, $distance);
		}
		$key = array_keys($results, min($results));		
		$office = $offices[$key[0]];

		return $office["name"];
	}

}