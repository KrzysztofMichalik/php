<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;
use Carbon\Carbon;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface{

	//Write your methods here
	public function calculate( $bookingDateTime, $responseDateTime, $officeHours)
	{
		$bookingDateTimeCarbon 	= Carbon::parse($bookingDateTime);
		$responseDateTimeCarbon 	= Carbon::parse($responseDateTime);
		$bookingDataOfficeHours 	= $officeHours[$bookingDateTimeCarbon->dayOfWeek];
		$responseDataOfficeHours 	= $officeHours[$responseDateTimeCarbon->dayOfWeek];		
		
		
		
		die();
	}
}